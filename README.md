# Community GraphiQL

GraphiQL Server that can be configured to run with a GraphQL server

## Setup

* Create a docker compose, and link a graphql server.  The service name has to
be `graphql`.

## Building

* This project pulls from https://github.com/graphql/graphiql to set up the
GraphiQL front end, and serves it via an nginx server.

* To update this project, the desired version must be specified as a build-arg.

Example build command:

```bash
docker build --build-arg VERSION=0.11.11 -t mprestifilippo/community-graphiql .
```

