FROM nginx:alpine

ARG VERSION
COPY ./graphiql-nginx.conf /etc/nginx/conf.d/default.conf
COPY ./index.html /usr/share/nginx/html/index.html
RUN sed -i "s/\$GRAPHIQL_VERSION/${VERSION}/g" /usr/share/nginx/html/index.html
